import pymongo
from account import mongodb_endpoint
from bson.objectid import ObjectId


def get_skip_amount(page_number):
    return (page_number - 1) * 9


def format_bookmarks_list(bookmarks):
    for bookmark in bookmarks:
        if str(bookmark['url']).startswith('http://') or str(bookmark['url']).startswith('https://'):
            bookmark['real_url'] = bookmark['url']
        else:
            bookmark['real_url'] = 'http://' + bookmark['url']

    return bookmarks


def get_all_bookmarks(page_number=1):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']
    bookmarks = col.find().sort('_id', pymongo.DESCENDING).skip(get_skip_amount(page_number)).limit(9)

    return format_bookmarks_list(list(bookmarks))


def get_bookmarks_from_category(category, page_number=1):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']

    bookmarks = col.find({'category': category}).sort('_id', pymongo.DESCENDING).skip(
        get_skip_amount(page_number)).limit(9)

    return format_bookmarks_list(list(bookmarks))


def get_all_categories(page_number=1):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']

    categories_db = col.find().sort('_id', pymongo.DESCENDING).skip(get_skip_amount(page_number)).limit(9)

    categories = []
    for category in categories_db:
        if category['category'] == '':
            continue

        categories.append(category)

    return categories


def get_recent_categories(limit):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']

    recents = col.find().sort('_id', pymongo.DESCENDING).limit(limit)
    if recents is None:
        return []
    return list(recents)


def get_recent_bookmarks(limit):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']

    recents = col.find().sort('_id', pymongo.DESCENDING).limit(limit)
    if recents is None:
        return []
    return list(recents)


def edit_bookmark(mongo_id, new_data):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']

    if new_data['category'] not in get_all_categories():
        add_category(new_data['category'])

    success = col.update_one({'_id': ObjectId(mongo_id)}, {'$set': new_data})
    if success is None:
        return False

    return True


def rename_category_name(old_category, new_category):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']
    col.find_and_modify({'category': old_category}, {'$set': {'category': new_category}})


def edit_category(mongo_id, new_data, old_name):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']

    if old_name != new_data['category']:
        rename_category_name(old_name, new_data['category'])

    bookmark = col.update_one({'_id': ObjectId(mongo_id)}, {'$set': new_data})

    if bookmark is None:
        return False

    return True


def delete_bookmarks_from_category(category_name):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']
    col.delete_many({'category': category_name})


def delete_category(mongo_id, category_name):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']
    deleted = col.delete_one({'_id': ObjectId(mongo_id)})
    delete_bookmarks_from_category(category_name)

    if deleted is None:
        return False

    return True


def delete_bookmark(mongo_id):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']
    deleted = col.delete_one({'_id': ObjectId(mongo_id)})

    if deleted is None:
        return False

    return True


def add_category(category, comment=''):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']
    exists = col.find_one({'category': category})

    if exists is not None:
        return False

    added = col.insert_one({'category': category, 'comment': comment})
    if added is None:
        return False
    return True


def add_bookmark(url, category, comment):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']
    added = col.insert_one({'url': url, 'category': category, 'comment': comment})
    if added is None:
        return False
    return True


def get_one_category(category_name):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']
    exists = col.find_one({'category': category_name})

    if exists is None:
        return None

    return exists


def search_bookmarks(query):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['bookmarks']
    results = col.aggregate([
        {
            "$search": {
                "text": {
                    "query": query,
                    "path": ["url", "category", "comment"]
                }
            }
        },
        {
            "$limit": 10
        },
        {
            "$project": {
                "_id": 1,
                "url": 1,
                "category": 1,
                "comment": 1
            }
        }
    ])

    return format_bookmarks_list(list(results))


def search_categories(query):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityBookmarks']
    col = db['categories']
    results = col.aggregate([
        {
            "$search": {
                "text": {
                    "query": query,
                    "path": ["category", "comment"]
                }
            }
        },
        {
            "$limit": 10
        },
        {
            "$project": {
                "_id": 1,
                "category": 1,
                "comment": 1,
            }
        }
    ])

    return list(results)


if __name__ == '__main__':
    print(get_all_bookmarks())
