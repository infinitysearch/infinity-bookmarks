from flask import Blueprint, render_template, request, redirect
import Bookmarks

bookmarksAPI = Blueprint('bookmarksAPI', __name__)


@bookmarksAPI.route('/')
def render_bookmarks():
    if 'delete' in request.args and 'type' in request.args:
        if request.args.get('delete') == 'success':
            return render_template('bookmarks_dashboard.html', success=True,
                                   msg='The ' + str(request.args.get('type')) + ' was successfully deleted.')
        elif request.args.get('delete') == 'failure':
            return render_template('bookmarks_dashboard.html', success=False,
                                   msg='There was an error deleting the ' + str(
                                       request.args.get('type')) + '. Please try again')

    return render_template('bookmarks_dashboard.html')


@bookmarksAPI.route('/all_bookmarks')
def render_all_bookmarks():
    if 'page_number' not in request.args:
        bookmarks = Bookmarks.get_all_bookmarks()
        return render_template('all_bookmarks.html', title='All', bookmarks=bookmarks, page_number=1)

    bookmarks = Bookmarks.get_all_bookmarks(page_number=int(request.args.get('page_number')))
    return render_template('all_bookmarks.html', title='All', bookmarks=bookmarks,
                           page_number=int(request.args.get('page_number')))


@bookmarksAPI.route('/all_categories')
def render_all_categories():
    if 'page_number' not in request.args:
        categories = Bookmarks.get_all_categories()
        return render_template('all_categories.html', categories=categories, page_number=1)

    categories = Bookmarks.get_all_categories(page_number=int(request.args.get('page_number')))
    return render_template('all_categories.html', title='All', categories=categories,
                           page_number=int(request.args.get('page_number')))


@bookmarksAPI.route('/bookmarks/category/<category>')
def render_category(category):
    category_db = Bookmarks.get_one_category(category)

    if category_db is None:
        return redirect('/')

    if 'page_number' not in request.args:
        bookmarks = Bookmarks.get_bookmarks_from_category(category, page_number=1)
        return render_template('all_bookmarks.html', title='Category: "' + category + '"', bookmarks=bookmarks,
                               is_category=True, category=category, comment=category_db['comment'],
                               id=str(category_db['_id']), page_number=1, type='category')

    bookmarks = Bookmarks.get_bookmarks_from_category(category, page_number=int(request.args.get('page_number')))
    return render_template('all_bookmarks.html', title='Category: "' + category + '"', bookmarks=bookmarks,
                           is_category=True, category=category, comment=category_db['comment'],
                           id=str(category_db['_id']), page_number=int(request.args.get('page_number')),
                           type='category')


@bookmarksAPI.route('/results/bookmarks', methods=['POST'])
def render_bookmark_search_results():
    post_data = dict(request.form)

    if 'query' not in post_data:
        return redirect('/')

    bookmarks = Bookmarks.search_bookmarks(post_data['query'])

    return render_template('all_bookmarks.html', title='Search: "' + str(post_data['query']) + '"',
                           query=post_data['query'],
                           bookmarks=bookmarks, pagination=False)


@bookmarksAPI.route('/results/categories', methods=['POST'])
def render_category_search_results():
    post_data = dict(request.form)

    if 'query' not in post_data:
        return redirect('/')

    categories = Bookmarks.search_categories(post_data['query'])

    return render_template('all_categories.html', title='Search: "' + str(post_data['query']) + '"',
                           category='Search Results', categories=categories, pagination=False)


@bookmarksAPI.route('/add_bookmark', methods=['GET', 'POST'])
def render_add_bookmark():
    if request.method == 'GET':
        category = ''
        if 'category' in request.args:
            category = request.args.get('category')
        categories = Bookmarks.get_all_categories()
        return render_template('add_bookmark.html', categories=categories, category=category)

    post_data = dict(request.form)

    url = ''
    category = ''
    comment = ''

    if 'url' in post_data:
        url = post_data['url']

    if 'category' in post_data:
        category = post_data['category']

    if 'comment' in post_data:
        comment = post_data['comment']

    if url == '' and category == '' and comment == '':
        return render_template('add_bookmark.html', success=False,
                               msg='You have to fill out at least one field to add a bookmark.')

    if category not in Bookmarks.get_all_categories():
        Bookmarks.add_category(category)

    added = Bookmarks.add_bookmark(url, category, comment)

    if added is False:
        return render_template('add_bookmark.html', success=False,
                               msg='There was an error uploading your bookmark. Please try again')

    return render_template('add_bookmark.html', success=True, msg='Bookmark added successfully.')


@bookmarksAPI.route('/add_category', methods=['GET', 'POST'])
def render_add_category():
    if request.method == 'GET':
        return render_template('add_category.html')

    post_data = dict(request.form)

    if 'category' not in post_data:
        return render_template('add_category.html', success=False,
                               msg='You have choose a category name before adding your bookmark.')

    if post_data['category'] == '':
        return render_template('add_category.html', success=False,
                               msg='You have choose a category name before adding your bookmark.')

    added = Bookmarks.add_category(str(post_data['category']), post_data['comment'])

    if added is False:
        return render_template('add_category.html', success=False,
                               msg='There was an error uploading your category. Please try again')

    return render_template('add_category.html', success=True, msg='Category added successfully.')


@bookmarksAPI.route('/edit_bookmark', methods=['POST'])
def render_edit_bookmark():
    post_data = dict(request.form)

    if 'edit' in post_data:
        categories = Bookmarks.get_all_categories()
        return render_template('edit_bookmark.html', id=post_data['id'], url=post_data['url'],
                               category=post_data['category'], comment=post_data['comment'], categories=categories)

    if 'update' in post_data:
        updated = Bookmarks.edit_bookmark(post_data['id'], {'url': post_data['url'], 'category': post_data['category'],
                                                            'comment': post_data['comment']})
        categories = Bookmarks.get_all_categories()
        if updated is False:
            return render_template('edit_bookmark.html', id=post_data['id'], url=post_data['url'],
                                   category=post_data['category'], comment=post_data['comment'], success=False,
                                   msg='There was an error updating your bookmark. Please try again.',
                                   categories=categories)
        return render_template('edit_bookmark.html', id=post_data['id'], url=post_data['url'],
                               category=post_data['category'], comment=post_data['comment'], success=True,
                               msg='Your bookmark was successfully updated.', categories=categories)

    if 'delete' in post_data:
        if 'sure' not in post_data:
            return render_template('delete_bookmark.html', id=post_data['id'], url=post_data['url'])

        deleted = Bookmarks.delete_bookmark(post_data['id'])
        if deleted is False:
            return redirect('/?delete=failure&type=bookmark')

        return redirect('/?delete=success&type=bookmark')

    return redirect('/')


@bookmarksAPI.route('/edit_category', methods=['POST'])
def render_edit_category():
    post_data = dict(request.form)

    if 'edit' in post_data:
        return render_template('edit_category.html', id=post_data['id'], category=post_data['category'],
                               comment=post_data['comment'])

    if 'update' in post_data:
        updated = Bookmarks.edit_category(post_data['id'],
                                          {'category': post_data['category'], 'comment': post_data['comment']},
                                          post_data['original'])
        if updated is False:
            return render_template('edit_category.html', id=post_data['id'], category=post_data['category'],
                                   comment=post_data['comment'], success=False,
                                   msg='There was an error updating your bookmark. Please try again.')

        return render_template('edit_category.html', id=post_data['id'], category=post_data['category'],
                               comment=post_data['comment'], success=True,
                               msg='Your category was successfully updated.')

    if 'delete' in post_data:
        if 'sure' not in post_data:
            return render_template('delete_category.html', id=post_data['id'], category=post_data['category'])

        deleted = Bookmarks.delete_category(post_data['id'], post_data['category'])
        if deleted is False:
            return redirect('/?delete=failure&type=category')

        return redirect('/?delete=success&type=category')

    return redirect('/')
