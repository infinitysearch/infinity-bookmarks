# Infinity Bookmarks
An easy way to store, comment on, and organize your links.

Note: If you do not want to self-host Infinity Bookmarks, this service will be included with Infinity Search Pro. 

## Self-Hosting Instructions
Anyone can host this on their own after just a few quick steps. 

#### Important Notice:
 This system is designed for single users and has nothing for adding 
multiple users or user authentication at the moment. It is meant for you to run the code 
on your computer and accessing the interface at http://localhost:5000 in your 
browser. 

### Required Software 
This system is built with the MongoDB database. You can either use its
cloud atlas connection or have MongoDB installed locally on your 
computer/server. 

Note: MongoDB has a free tier of their cloud storage that holds up to 500 mb of data. 

If you do the cloud setup, go to your account.py file and set the value to your MongoDB connection 
string that can be found on your MongoDB dashboard. If you are running mongo locally, change 
all the occurrences in the Bookmarks.py file of "client = pymongo.MongoClient(mongodb_endpoint)" to 
"client = pymongo.MongoClient()". 

### Setup 
1. Clone or download this repository 
2. Install Python3 (preferrably Python3.7) along with pip3
3. Change into this directory 
4. Run this in the command line:
    ```shell script
    pip3 install -r requirements.txt
    ```
5. To make your MongoDB bookmarks and categories searchable, you will need to login to your MongoDB dashboard, go to the 
bookmarks and categories collection, go to the search tab, and then enable the search index of your collection. After this, you will be 
able to search through your bookmarks and categories. 
6. Run the wsgi.py file in your IDE or with:
    ```shell script
    python3 -m wsgi 
    ```
7. Now, Infinity Bookmarks should be up and running at http://localhost:5000 or http://127.0.0.1:5000!

