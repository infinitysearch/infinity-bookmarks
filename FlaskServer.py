from flask import Flask, send_from_directory
import os

app = Flask(__name__)

from websiteAPI.bookmarksAPI import bookmarksAPI

app.register_blueprint(bookmarksAPI)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run(debug=False)
